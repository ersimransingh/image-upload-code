import React, { Component } from 'react';
import { View, Text , Button, Image} from 'react-native';
import firebase from 'react-native-firebase';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
    isUploading:false,
    all:{}
  },
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uri:'https://moorestown-mall.com/noimage.gif'
    };
  }
  openpicker(){
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
     
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({uri:response.uri});
        const source = { uri: response.uri, all :response };
        this.makeBlob(response.uri, response.fileName, response.type)
     
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
     
        this.setState({
          avatarSource: source,
        });
      }
    });
  }
  makeBlob(uri, fileName, type) {
    this.setState({ isUploading: true })
    const image = uri
    const Blob = RNFetchBlob.polyfill.Blob
    const fs = RNFetchBlob.fs
    window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
    window.Blob = Blob
    const imageId = new Date().getTime();
    const imageRef = firebase.storage().ref(fileName)
    let uploadBlob = null
    
    fs.readFile(image, 'base64')
        .then((data) => {
            return Blob.build(data, { type: `${type};BASE64` })
        })
        .then((blob) => {
            uploadBlob = blob
            return imageRef.put(blob._ref, { contentType: type })
        })
        .then(() => {
            uploadBlob.close()
            return imageRef.getDownloadURL()
        })
        .then((url) => {
            
            this.setState({ uri: url, isUploading: false })

        })
        .catch((error) => {
            console.log(error);
            this.setState({ isUploading: false })
            Toast.show({
                text: 'Unable to update profile picture',
                type: "danger",
                duration: 3000
            })
        })
}
  uploadFile(){
    
    
  }
  render() {
    return (
      <View>
        <Text> App </Text>
       
        <Button
          title="PICKER"
          onPress={() => this.openpicker()}
        />
        <Text>{this.state.uri}</Text>
        <Image 
          style={{width:200, height:200}}
          source={{uri:this.state.uri}}
        />
        <Button 
          title="Upload"
          onPress={() => this.uploadFile()}
        />
      </View>
    );
  }
}

export default App;
